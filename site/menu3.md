@def title = "WebWork"

# Homework

![Alt Text](/assets/HW.jpg)





## Useful WeBWork Info

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) 
* [Click here](https://www.amazon.com/clouddrive/share/LYteFM8qahwmYBIX977VIVzz0QLglHdl1LIMCRKa0BG) to see a tutorial on how to use WeBWork
* [Click here](https://www.amazon.com/clouddrive/share/z6xFHyYgtF62mzT03buAib4NP2L7seZoWDyqBoBTFKQ) to read the tutorial on entering answers in WeBWork


## WebWork assignments

* [HW 1](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, January 27th. 



