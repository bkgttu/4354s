@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of Differential Equations! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 4354: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)



## Things to be known:
Course syllabus [lives here!](https://tinyurl.com/2zkdhnoq)


* **Lecture:** TT: 11:00AM -12:20PM,  
    - Live class session via ZOOM: [Click here!!!]() 
* **Instructor:** Prof. Bijoy Ghosh (bijoy.ghosh@ttu.edu)
* **Teaching Assistant:** Indika Gunawardana (Indika-Gihan-Gunawardana.Dewage@ttu.edu)
* **Office Hours through Zoom:**  TR: 2:00PM - 3:00PM
* **WebWork:**  [Click here](https://webwork.math.ttu.edu/webwork2/spr21bghoshm4354s002/)


## Announcements

![Alt Text](/assets/news.jpg)


* Quiz 1: Feb 5
* Exam 1: Feb 24, Feb 25
* Quiz 2: March 12
* Exam 2: March 31, April 1
* Quiz 3: April 16
* Final Exam: To be announced






