@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)
## Week 15

* Tuesday, Apr. 27th
    - [Lecture note](https://tinyurl.com/53aa8dnd)
    - Missed the lecture, No worries [click here](https://tinyurl.com/8fkx8dku)



## Week 14

* Tuesday, Apr. 20th
    - [Lecture note](https://tinyurl.com/ay9era42)
    - Missed the lecture, No worries [click here](https://tinyurl.com/2xaj82y9)



## Week 13
* Thursday, Apr. 15th
    - [Lecture note 20](https://tinyurl.com/44z9uk3m)
    - [Lecture note 19](https://tinyurl.com/4436d7b7)
    
* Tuesday, Mar. 13th
    - [Lecture note](https://tinyurl.com/yh6m4wm3)
    - [Lecture note](https://tinyurl.com/3s3cajep)
    - Missed the lecture, No worries [click here](https://tinyurl.com/3w4zmjhm)


## Week 12
* Thursday, Apr. 08st
    - [Lecture note 18](https://tinyurl.com/y8fjpvt6)
    - [Lecture note 17](https://tinyurl.com/7tsf3xxe)
    - Missed the lecture, No worries [click here](https://tinyurl.com/x5sd99ve)
    
* Tuesday, Mar. 06th
    - [Lecture note](https://tinyurl.com/7tsf3xxe)
    - Missed the lecture, No worries [click here](https://tinyurl.com/rdbfj3e4)


## Week 11
* Thursday, Apr. 01st
    - [Lecture note](https://tinyurl.com/u44eyu3p)
    - Missed the lecture, No worries [click here](https://tinyurl.com/2rpm5erx)
    
* Tuesday, Mar. 30th
    - [Lecture note](https://tinyurl.com/4pu4dshs)
    - Missed the lecture, No worries [click here](https://tinyurl.com/jkbnak6s)




## Week 10
* Thursday, Mar. 25th
    - [Lecture note](https://tinyurl.com/4pu4dshs)
    - Missed the lecture, No worries [click here](https://tinyurl.com/y3ks6m2y)
    
* Tuesday, Mar. 23rd
    - [Lecture note](https://tinyurl.com/bw3p6uru)
    - Missed the lecture, No worries [click here](https://tinyurl.com/3nmcynpp)



## Week 9
* Thursday, Mar. 18th
    - [Lecture note](https://tinyurl.com/bw3p6uru)
    - Missed the lecture, No worries [click here](https://tinyurl.com/3j59ud5h)
    
* Tuesday, Mar. 16th
    - [Lecture note](https://tinyurl.com/6549fcyt)
    - Missed the lecture, No worries [click here](https://tinyurl.com/ysvj6a7a)




## Week 8
* Thursday, Mar. 11th
    - [Lecture note](https://tinyurl.com/6549fcyt)
    - Missed the lecture, No worries [click here](https://tinyurl.com/ysvj6a7a)
    
* Tuesday, Mar. 9th
    - [Lecture note](https://tinyurl.com/3ksa3zw2)
    - Missed the lecture, No worries [click here](https://tinyurl.com/rvv5j453)


## Week 7
* Thursday, Mar. 04th
    - [Lecture note](https://tinyurl.com/3ksa3zw2)
    - Missed the lecture, No worries [click here](https://tinyurl.com/y3a7j3h4)
    
* Tuesday, Mar. 2nd
    - [Lecture note](https://tinyurl.com/4s4vxfmx)
    - Missed the lecture, No worries [click here](https://tinyurl.com/kja58pcx)


## Week 6

* Thursday, Feb. 25th
    - [Lecture note](https://tinyurl.com/zwypa8ss)
    - Missed the lecture, No worries [click here](https://tinyurl.com/5cpvf72j)
    
* Tuesday, Feb. 23rd
    - [Lecture note](https://tinyurl.com/2psxs942)
    - Missed the lecture, No worries [click here](https://tinyurl.com/t6k5hxww)

## Week 5 

* Thursday, Feb. 18th
    - [Lecture note](https://tinyurl.com/yx72pqxc)
    - Missed the lecture, No worries [click here](https://tinyurl.com/42nq5n7x)


* Tuesday, Feb. 16th
    - [Lecture note](https://tinyurl.com/njtm36ak)
    - Missed the lecture, No worries [click here](https://tinyurl.com/wrmaubk)

## Week 4 

* Tuesday, Feb. 09th
    - [Lecture note](https://tinyurl.com/1lnfnwaf)
    - Missed the lecture, No worries [click here](https://tinyurl.com/kblhzmrx)


## Week 3 

* Thursday, Feb. 04th
    - [Lecture note](https://tinyurl.com/4kh8lmdm)
    - Missed the lecture, No worries [click here](https://tinyurl.com/1e2m11vj)

* Tuesday, Feb. 02nd
    - [Lecture note](https://tinyurl.com/ufwex3t2)
    - Missed the lecture, No worries [click here](https://tinyurl.com/2anrwufe)

## Week 2 

* Thursday, Jan. 28th
    - [Lecture note](https://tinyurl.com/an3siq2n)
    - Missed the lecture, No worries [click here](https://tinyurl.com/2jlf9fsp)

* Tuesday, Jan. 26th
    - [Lecture note](https://tinyurl.com/1lvcyw1k)
    - Missed the lecture, No worries [click here](https://tinyurl.com/3aptbup2)


## Week 1 

* Thursday, Jan. 21st
    
    - Missed the lecture, No worries [click here](https://tinyurl.com/18vvjxag)

 



